import React, { Component } from 'react';
import Header from './header';
import Slider from './slider';
import PhoneList from './phoneList';
import LaptopList from './laptopList';
import Footer from './footer';
class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Slider/>
                <PhoneList/>
                <LaptopList/>
                <Footer/>
            </div>
        );
    }
}

export default Home;