import React, { Component } from 'react';
import Phone from './phone';

class PhoneList extends Component {
    render() {
        return (
        <div className="phoneList bg-dark py-4">
          
            <h3 className="text-center text-white py-4"  >Best Smartphone</h3>
            <div className="row">
              <Phone />
              <Phone />
              <Phone />
              <Phone />
            </div>
          
        </div>
        );
    }
}

export default PhoneList;