import React, { Component } from 'react';
import Laptop from './laptop';
class LaptopList extends Component {
    render() {
        return (
            <div>
                <h3 className="text-center py-4">Best Laptop</h3>
                <div className="row">
                    <Laptop/>
                    <Laptop/>
                    <Laptop/>
                    <Laptop/>
                </div>
            </div>
        );
    }
}
export default LaptopList;